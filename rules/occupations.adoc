---
layout: page
permalink: /rules/occupations
---
= Occupations

Before every character became an adventurer, they had a job.
Roll d100 on this table to establish their previous occupation, along with a weapon and item of their trade.
Weapon types are indicated as (L)ight (M)edium or (H)eavy.

.Occupations Table
[cols="^1,2,2,2", options="header"]
|===
|1d100 | Occupation | Weapon | Item

|1|Actor|Walking Cane (L)|Pocket Makeup Kit
|2|Alchemist|Knife (L)|Oil Flask
|3|Animal Trainer|Club (M)|link:#LoyalAnimal[Loyal Animal]
|4|Armorer|Ball-Peen Hammer (L)|Plate Helmet
|5|Astrologer|Ritual Dagger (L)|Book of star charts
|6|Baker|Rolling Pin (L)|3 Rations
|7|Barber|Razor (L)|Needle and ball of wax thread (100')
|8|Beekeeper|Honey Scraper (L)|Jar of Honey
|9|Blacksmith|Hammer (M)|Iron Tongs
|10|Braiser|Ball-Peen Hammer (L)|Small brass trinket
|11|Brewer|Paddle (H)|Skin of ale
|12|Butcher|Cleaver (M)|Live Lamb
|13|Carder|Cudgel (L)|Bundle of Wool
|14|Carpenter|Hammer (L)|Bag of nails (20)
|15|Cartwright|Hammer (L)|Handcart
|16|Chalk-Cutter|Chisel (L)|5 pieces of chalk
|17|Chandler|Knife (L)|3 Candles
|18|Cheesemonger|Cudgel (L)|1 10" wheel cheese
|19-23|Clergy|Cudgel (L)|Holy book or symbol
|24|Clerk|Knife (L)|Quill & Ink
|25|Clockmaker|Cudgel (L)|Precision Tools
|26|Cobbler|Hammer (L)|Awl & leather cord (20')
|27|Cook|Knife (L)|Herb pouch
|28|Cooper|Hammer (L)|Barrel (0 slots)
|29|Dyer|Cudgel|3 vials of dye
|30-39|Farmer|Pitchfork (H)|Sack of link:#Crops[Crops]
|40|Farrier|Cudgel (L)|3 horseshoes
|41-45|Fisherman|Fishing Spear (M)|Net
|46|Fletcher|Bow|20 arrows
|47|Fortune-Teller|Ritual Dagger (L)|Tarot Deck
|48|Glassblower|Cudgel (L)|3 glass vials
|49|Glove-maker|Hand Mold (M)|2 pairs of gloves
|50|Gravedigger|Shovel (H)|2d4 copper coins
|51|Grocer|Cudgel (L)|3 rations
|52-54|Guard|Spear (M)|Manacles
|55|Haberdasher|Scissors (L)|Fine Clothes
|56|Herbalist|Cudgel (L)|Mortar & Pestle
|57-61|Herder|Crook (H)|Loyal dog
|62|Hunter|Bow|Trap & 20 arrows
|63|Illustrator|Knife (L)|Ink & Quill
|64|Jailer|Blackjack (L)|Manacles
|65|Jeweler|Cudgel|10' of wire scraps
|66|Lawyer|Dueling Sword (M)|Ink & Quill
|67|Leatherworker|Knife (L)|Leather Helmet
|68|Locksmith|Knife (L)|Precision tools
|69|Mason|Cudgel (L)|Trowel & Square
|70|Mercer|Scissors (L)| 1 bolt of silk
|71-73|Mercenary|Sword (M)|Shield
|74|Midwife|Scissors (L)|3 yards of soft cloth
|75|Miller|Cudgel (L)|Sack of Flour
|76|Miner|Pick (M)|Iron Helm with candle holder
|77|Needlemaker|Cudgel (L)|20 needles in pouch
|78|Ostler|Pitchfork (H)|Bit & Bridle
|79|Painter|Paint Knife (L)|Small case of Paints
|80|Parchment-maker|Rolling Pin (L)|5 pages of parchment
|81|Plasterer|Cudgel (L)|Bucket & Brush
|82|Porter|Cudgel (L)|Hand Cart
|83|Potter|Cudgel (L)|Sack of Clay
|84|Prostitute|Shiv (S)|Fancy undergarments
|85|Rat Catcher|Cudgel (L)|Loyal small Dog
|86|Rope-maker|Sheers (M)|50' of rope
|87|Sailor|Cudgel (L)|3 yards of sailcloth
|88|Scribe|Cudgel (L)|Ink & Quill
|89|Servant|Cudgel (L)|2d6 silver
|90|Shipwright|Hammer (M)|Sack of hardened tar chunks
|91|Shrubber|Sheers (M)|Leather gloves
|92|Stonecutter|Hammer (M)|Chisel
|93|Tailor|Scissors (L)|Fancy Clothes
|94|Tanner|Knife (L)|2 yards of leather
|95|Tax Collector|Short Sword (L)|Lockbox
|96|Teamster|Cudgel (L)|Pony & Cart
|97|Thatcher|Sickle (M)|100' spool of firm wire
|98|Vinter|Cudgel (L)|Fortified wine
|99|Weaver|Shuttle (L)|Blanket
|100|Woodcutter|Axe (H)|200' of hemp cord

|===

== Additional Tables

Some occupations have semi-random items.
These tables are for determining which item your particular character has.

.Crops Table
[[Crops]]
[cols="^1,2",options="header",width="25%"]
|===
|1d6
|Crop

|1
|Carrots

|2
|Beats

|3
|Cabbages

|4
|Potatoes

|5
|Oats

|6
|Beans

|===

.Loyal Animal Table
[[LoyalAnimal]]
[cols="^1,2",options="header",width="25%"]
|===
|1d6
|Animal

|1-2
|Dog

|3-5
|Pony

|6
|Hawk
|===