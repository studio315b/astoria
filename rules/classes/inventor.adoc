= Inventor

Inventors have a number of bonus quick access inventory slots equal to [templates].

[alphaupper]
. Inventor Dice (d6), Gizmos, Random Invention
. Inventor Dice (d8), Just the Tool
. Inventor Dice (d10), Perfected Gizmo
. Inventor Dice (d12), Signature Gizmo

== Inventor Dice

You have 2 x [template] inventor dice.
These dice are [locked] into items to produce effects.
If a die is [locked], it is not restored after a nights rest.
Dice that are unlocked (via destruction or disassembly) are restored after the next nights rest.

== Gizmos

You may spend 1 hour, 2 units of *junk* and any number of inventor dice to produce a magical gizmo.
The gizmo is one of these kinds (players choice):

* *Ray*: Effect applies in a 5 foot by 50 foot beam. If it makes sense, the beam reflects off surfaces.
* *Grenade*: Effect applies in a 10 foot sphere around the gizmo. To land accurately, it requires an attack roll -1 for every 5' beyond 30'. On a miss, it lands 10 feet away in a random direction.
* *Cone*: Effect applies in a 15 by 15 cone in front of the inventor.
* *Trap*: Effect applies in a 10 foot sphere around the gizmo on an external trigger (including manual, up to 100' away)

Gizmo's always have a single effect, and deteriorate with use.

=== Damage

Each time you use a gizmo, make a checkmark next to it, and roll d10.
if the result is equal to or less than the number of checkmarks, the gizmo works, but is now _broken_.
A _broken_ gizmo can be repaired with 1 hour and a use of junk to remove the broken condition and half (rounded down) of the checkmarks.
A _broken_ gizmo can also be used, but if you roll less than the number of checkmarks again, the device fails and is unusuable.

Unusable gizmos can be recycled into 1 unit of junk.
Gizmos must be maintained, or they lose their magical properties.
Maintaining all of your gismos takes 1 hour and 1 unit of junk.
A gizmo that goes 3 days without being maintained falls apart and the inventor dice used to make it are restored.

=== Junk

A _Box of Junk_ is an item that costs 2sp, takes up 2 inventory slots, and contains 10 units of junk.

Loose junk can be scavenged, but take up 1/3 of an inventory slot.

== A. Random Invention

You are able to create gizmos, but are unable to control what exactly they do. Upon first using the gizmo, roll all Inventor dice spent, and choose an effect from the numbers rolled:

. *Fire*: [Sum] damage and unattended objects catch fire
. *Ice*: [Sum]/2 damage and targets move at half speed and act at -2 for [dice] rounds.
. *Lightning*: [Sum]/2 damage and targets are paralyzed for [dice] rounds.
. *Sleep*: Roll under [Sum] for each target, on a success, they fall asleep for [dice] turns.
. *Light*: For [Sum] turns, emits bright light in the affected area, and dim light twice that distance. The device can be deactivated at any time during the duration.
. *Poison* Roll under [Sum] for each target, on a success, they take [dice] damage each round until the poison is neutralized.
. *Corrosive*: Eats through flesh and armor, destroying any armor targets are wearing and dealing a portion of damage based on the armor ([sum] for no armor, [sum]/2 for light armor, [sum]/3 for medium armor, and [sum]/4 for heavy armor)
. *Anti-Magic*: Disables all magic within the field and prevents casting while under it's effect. Spell cannot penetrate the field.
. *Necrotic*: Deals [sum]*2 damage to living creatures. No effect on machines and heals [sum] for undead
. *Sound*: Deals [sum]/2 damage to all targets that can hear, and deafens them for [dice] turns. Brittle objects are immediately destroyed.
. *Vampiric*: Each round, roll under [sum] for each target, on a success, they take [dice] damage and the effect lasts 1 more round. With no targets, the gizmo lasts [dice] rounds.
. *Weird*: The gizmo has a strange effect. Come up with something.

== B. Just The Tool

You always happen to have just the right tool (provided it can fit in a 6" sphere) handy.
These tools are hidden all over your person and take up no space in your inventory.

== C. Perfected Gizmo

You may spend an entire day working on a single gizmo, if you do, you now roll a d20 when testing for gizmo damage.

== D. Signature Gizmo

You choose your gizmo's effect when you make it, instead of having to wait until it's first use.

Additionally, you may choose a single gismo with 4 or less ID that no longer can be damaged.
It still requires maintenance, and you may choose to scrap it to make another device your signature device.