= Fighter

Fighters gain +1 HP for each template they have (max HP of 20 still applies.)

[upperalpha]
. Signature Weapon & Gambits
. Notches
. Extra Attack & Second Signature Weapon
. Rally & Shake It Off 

== A. Signature Weapon

Choose a distinct weapon (such as longswords or bows.) 
As long as you are wielding that weapon, your attack and defense stats get +1.

Changing your signature weapon is possible.
When you switch weapon types, make a checkmark each time you hit with the new weapon, then roll d10, if you roll less than the number of checkmarks, your signature weapon is now the new weapon.
Erase all checkmarks afterwords.

As part of C, you gain a second signature weapon.

== A. Gambits

As part of a successful attack, you may preform a Gambit.
Declare a success effect (Disarm opponent) and a failure effect (Opponent gets a free attack.)
If the Referee accepts the gambit, make a level check (roll under 10+level,) and apply the appropriate effect.

== B. Notches

Your signature weapon gains it's own level, earning awakening points from kills made with it.
Your weapon's level can never exceed [templates], and stops gaining awakening points until [templates] increases.
Upon levelup, you may choose one of the following features:

* Increased Critical: You critical on a 1 or 2
* Bloody: Roll your damage dice twice, and take the higher value.
* Cleave: When you kill an enemy, you may make an additional attack on another nearby enemy
* Sentinel: Once per round, you may make a free attack against an enemy who retreats from your reach.
* Parry: Once per day, you can parry an attack and reduce it's damage by 1d12 OR sunder your shield to reduce the damage by 12 instead of 1d12.
* Riposte: Requires *Parry*, when you parry, you get a free attack, you may also parry an additional time each day.
* Guardian: Once per day, when a nearby ally is hit, you may sunder your shield on their behalf, reducing the damage accordingly.

Each feature can only be chosen once.

== C. Extra Attack

On your turn, you may make an extra attack.

== D. Rally

Twice per day, you may apply a 1d6 bonus to morale checks of hirelings before it is rolled or heal any allies within hearing range 1d6 damage through a motivational battle cry.

== D. Shake it off

Once per combat, as an action, you may heal to full health.

== Victims

* link:https://twogoblinsinatrenchcoat.blogspot.com/2019/03/fight-glog-fighter.html[Two Goblins in a Trenchcoat]: Tricky is a good substitute for DCC's Mighty Deeds, one of my favorite parts of that system. Part of my rally is based on this one, but I can't seem to find where I found the rest of it. Hit me up on Reddit or Discord if you recognize where I might have gotten it from.
* link:https://princesses-and-pioneers.tumblr.com/post/183755011838/fighter[Princesses & Pioneers]: The roll under variant of notches feels better than the arbitrary numbers. Took Sentinel as a notches bonus to increase variety among fighters. 
* link:https://github.com/valzi/GLOG-classes/wiki/Martial_Fighter-Magnificentophat[Valzi's Wiki]: Riposte looked cool.
* link:http://www.swordsandstorytellers.com/2018/09/glog-fighter-variant.html[Swords and Storytellers]: Shake It Off turns fighters into unkillable machines. I like it!