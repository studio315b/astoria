= Cultist

Each time a cultist levels, when testing an attribute, if their patron embodies that attribute, treat it as 4 lower for testing.
// for Banta-Fai, instead treat any attribute as 2 lower

[alphaupper]
. +1 MD, Consult, 2 random spells (1-6)
. +1 MD, Aspect, 1 chosen spell (1-6)
. +1 MD, Flaw, 1 chosen spell (1-8)
. +1 MD, Emissary, 1 chosen spell (1-10)

== A. Consult

Ancient as they are, the Seven know much about the world and it's history.
You may invoke your patron and ask it a singular question, which they will answer in the way most helpful to it's interests.
After answering the question, put a checkmark here.
The next time you invoke a consult, roll d10, if you roll equal to or lower than the number of checkmarks, the patron does not show up, as it has better things to do than bend to your whims today.

Erase any checkmarks after a nights rest.

== B. Aspect

Each patron grants a different aspect to it's champions:

=== Aeto-Krah (Mechanical Understanding)

You may spend a turn observing a mechanical process, or studying a device, then invoke your patron.
You gain a perfect understanding of how the process or device works, and have a +4 to interacting with said device.

You may make a consult check, then add a checkmark to understand the process or device as a free action, and automatically succeed in avoiding harm from that process or device this turn.
In the case of traps, this can cause you to move up to 20' into the safest position.

=== Irad-Mann (Blessed Weapon)

You may destroy a weapon, willing it's spirit to Irad-Mann.
Once this is done, you may retrieve this weapon by invoking your patron, and should it unwillingly leave your hands, it disappears, returning to safety in her care.
You may also call upon your patron to grant you additional power.
Doing so requires a consult check, and adds a checkmark, but grants a +2 to attack and always deals maximum damage for a number of round equal to [templates].

=== Obot-Bach (Zone of Truth)

After spending 1 turn with someone, you are able to tell when they're lying.

You may also call upon your patron to prevent lies entirely.
This effect lasts 1 turn and takes a consult.

=== Thedas-Pal (Resistance)

You may reroll failed saves or defenses and take the new result.
Once this effect has turned a failure into a success, you may not use it again until after a nights rest.

You may spend a consult to reduce the damage of an attack or the duration of an effect by half.

== C. Flaw

As you become tightly intwined with your patron, you suffer their flaws as your own.

* *Aeto-Krah (Obsession)* Designate a project. You are intently focused on bringing it to completion. Acting in ways that do not directly aid in this goal causes you mental anguish, resulting in a -1 to all rolls for each day you've spent not working on the project. This can be mitigated by spending an hour working on a smaller project, such as a mechanical device or a carving.
* *Irad-Mann (Power)* You are driven to achieve greatness. You have a -1 to rolls in situations where you are noticeably weak, or are acting in a submissive nature.
* *Obot-Bach (Honesty)* You cannot lie. You must make a save whenever you witness someone lying to call them out on it.
* *Thedas-Pal (Daredevil)* You must take risks. In a situation where a safe and risky option is presented, you must make a save to take the safe path. 

== D. Emissary

You are now an emissary of your deity, and have even more power.
You may make 3 check marks and make a consult check, on a success, you summon an avatar of your patron to aid you for 1 turn.

== Spells

=== Aeto-Krah

. *Mechanical Construct* E: You may create a magical construct with [dice] HD and [sum] HP.
This construct obeys your commands and is able to preform simple tasks, but is unable to apply more than 20lbs of force.
The construct acts with you during combat and attacks with a Light weapon (d6)
This construct will collapse after [dice] days, but can last until your death if a die is [locked].
. *Blur*: D: [sum] turns E: Gain +[dice] to defense
. *Fleetfooted*: D: [Dice] turns E: Your movement is increased by 5 * [Sum] feet, and you may travel through occupied areas as if they were difficult terrain.
. *Control Device*: R: 50' D: [Dice] turns E: You may manipulate a mechanical device you are aware of with your mind, including unlocking locks, lifting portcullises, or springing traps.
. *Find Object* R: [sum] miles E: By focusing on the details of a particular item you are aware of, you know it's exact location, provided it is within range.
. *Destroy Device* R: 50' E: You deal [sum] damage to the internals of a device. If you know how it works, you may apply this damage to a known weak point (effectively doubling the damage) leaving it inoperable.
. TODO: AK7
. TODO: AK8
. TODO: AK9
. TODO: AK0

=== Irad-Maan

. *Invoke Fire* R: 50' T: [Dice] * 5' sphere D: Instant E: Deal [Sum] damage to all targets in range, and ignite unattended objects
. *Shatter* R: 50' T: 1 object D: Instant E: Destroy the object, it makes a terrible noise, dealing 1d6 damage to anyone within [dice] * 10' of the target
. *Dominate* R: 50' T: 1 person D: [Dice] days E: Roll against [Sum], on a success, the target yields to you and fears you for the duration. They will do whatever they can to escape your presence.
. *Haste* R: Touch T: 1 target D: [dice] rounds E: Target moves twice as fast for the duration. If at least 3 dice are invested, they can take any extra action each round instead, such as making another attack.
. *Challenge* R: 50' T: 1 target D: [dice] minutes E: The target must only attack you, and you must only attack the target or suffer a -2 penalty.
. *Jump* R: Self T: Self D: 1 Hour E: You may jump [dice] times your height in any direction.
. TODO: Irad 7
. TODO: Irad 8
. TODO: Irad 9
. TODO: Irad 10

=== Obot-Bach

. *Comprehend Languages* D: [Dice] turns E: You can read, write and speak any language that you interact with during this time.
. *Read Mind* R: 50' T: 1 person D: [dice] turns E: Target makes a save vs [sum] or you may read their conscious thoughts.
. *Scry* D: [Dice] turns E: You must roll against [sum]. On a success, you see your target, a 20' sphere around them, and sense anything they could sense. On a failure, you only gain 1 sense (smell, sight, taste, sound, touch) and they get the feeling they're being watched.
. *Truesight* D: [sum] rounds E: You can see invisible things and all illusions are dissolved. You can also see in total darkness up to 60'
. *Know Person* T: 1 person R: Touch E: You touch a person, and know their life story, the names of friends and enemies, where they grew up, and who they loved. You gain +2 defense against this person until they fundamentally change, but are at -2 to attack them, as the personal bond makes it difficult to harm them. If this person is unwilling, you must roll against [sum] or nothing happens.
. *Witness History* T: 1 object E: You know the history of the held object, going back [sum] years. You know the rough date of any change to it, and roughly where it traveled. You are aware of it's owners during this time, and should you meet one of these people, you immediately know it belonged to them.
. TODO: OB7
. TODO: OB8
. TODO: OB9
. TODO: OB0

=== Thedas-Pal

. *Hold Fast* R: Touch T: 1 inanimate target D: [dice] turns E: The object cannot move or be moved. If the ground is cut from below it, it does not fall, hitting it causes the object to bouse off with perfect redirection of force, and makes no noise.
. *Thick Skin* T: Self D: [sum] rounds E: Reduce all damage dealt to you by [dice] (minimum 0).
. *Endure Elements* R: 50' sphere D: [Dice] watches E: You, and allies in range are insulated from the worst of ambient natural effects. You do not get overheated or cold, wind avoids you and you cannot be struck by natural lightning.
For the duration of the watch, you travel as if you were travelling on a pleasant spring day.
. *Indomitable Might* R: 50' sphere D: [sum] rounds E: You, and allies within range get +[dice] to saves against mind altering effects, and may resave every turn. Enemies get -[dice] to similar effects, and must make a morale check (at -[dice]).
. *Manipulate Stone* R: 50' T: [dice] * 10 cubic ft of stone D: 1 round E: You may manipulate stone into a simple design including walls and holes. The stone retains it's strength after the manipulation.
. *Resistance to Element* R: 50' T: [dice] people D: [sum] rounds E: Targets take half damage from effects of a specific element (fire, ice, ect) designated at casting.
. TODO: TP7
. TODO: TP8
. TODO: TP9
. TODO: TP0