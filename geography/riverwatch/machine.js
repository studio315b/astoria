/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./machine.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./machine.ts":
/*!********************!*\
  !*** ./machine.ts ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\nexports.parseProgram = exports.print10 = exports.print4 = exports.runProgram = exports.buildMachine = exports.runInst = exports.Ops = void 0;\nvar Ops;\n(function (Ops) {\n    Ops[Ops[\"AND\"] = 0] = \"AND\";\n    Ops[Ops[\"AND*\"] = 1] = \"AND*\";\n    Ops[Ops[\"ADD\"] = 2] = \"ADD\";\n    Ops[Ops[\"ADD*\"] = 3] = \"ADD*\";\n    Ops[Ops[\"OR\"] = 4] = \"OR\";\n    Ops[Ops[\"OR*\"] = 5] = \"OR*\";\n    Ops[Ops[\"SUB\"] = 6] = \"SUB\";\n    Ops[Ops[\"SUB*\"] = 7] = \"SUB*\";\n    Ops[Ops[\"NOT\"] = 8] = \"NOT\";\n    Ops[Ops[\"RST\"] = 9] = \"RST\";\n    Ops[Ops[\"SAVE\"] = 10] = \"SAVE\";\n    Ops[Ops[\"LOAD\"] = 11] = \"LOAD\";\n    Ops[Ops[\"JEQ-\"] = 12] = \"JEQ-\";\n    Ops[Ops[\"JEQ+\"] = 13] = \"JEQ+\";\n    Ops[Ops[\"JNE-\"] = 14] = \"JNE-\";\n    Ops[Ops[\"JNE+\"] = 15] = \"JNE+\";\n})(Ops = exports.Ops || (exports.Ops = {}));\nfunction parseCommon(value) {\n    return {\n        mode: Math.floor(value / 1024),\n        value: value % 1024\n    };\n}\nfunction runInst(machine, inst, len) {\n    switch (inst.op) {\n        case Ops.AND:\n            machine.working &= inst.data;\n            break;\n        case Ops[\"AND*\"]:\n            machine.working &= machine.registers[inst.data % 8];\n            break;\n        case Ops.ADD:\n            machine.working += inst.data;\n            break;\n        case Ops[\"ADD*\"]:\n            machine.working += machine.registers[inst.data % 8];\n            break;\n        case Ops.OR:\n            machine.working |= inst.data;\n            break;\n        case Ops[\"OR*\"]:\n            machine.working |= machine.registers[inst.data % 8];\n            break;\n        case Ops.SUB:\n            machine.working -= inst.data;\n            break;\n        case Ops[\"SUB*\"]:\n            machine.working -= machine.registers[inst.data % 8];\n            break;\n        case Ops.NOT:\n            machine.working = 1023 - machine.working;\n            break;\n        case Ops.RST:\n            machine.working = 0;\n            break;\n        case Ops.SAVE:\n            machine.registers[inst.data % 8] = machine.working;\n            break;\n        case Ops.LOAD:\n            machine.working = machine.registers[inst.data % 8];\n            break;\n        case Ops[\"JEQ-\"]:\n            machine.pc -= (machine.working == 0) ? inst.data : 0;\n            break;\n        case Ops[\"JEQ+\"]:\n            machine.pc += (machine.working == 0) ? inst.data : 0;\n            break;\n        case Ops[\"JNE-\"]:\n            machine.pc -= (machine.working != 0) ? inst.data : 0;\n            break;\n        case Ops[\"JNE+\"]:\n            machine.pc += (machine.working != 0) ? inst.data : 0;\n            break;\n        default:\n            throw `Unexpected op code ${inst.op} at ${machine.pc}`;\n    }\n    machine.working %= 1000;\n    if (machine.working < 0) {\n        machine.working + 1000;\n    }\n    machine.pc++;\n    machine.ops++;\n    return machine;\n}\nexports.runInst = runInst;\nfunction buildMachine(working, registers) {\n    return {\n        working,\n        registers,\n        pc: 0,\n        ops: 0\n    };\n}\nexports.buildMachine = buildMachine;\nfunction runProgram(program, working, ...registers) {\n    let machine = buildMachine(working, registers);\n    while (machine.pc < program.length) {\n        machine = runInst(machine, program[machine.pc], program.length);\n        if (machine.ops > 100000000) { // stop at 100 mil\n            machine.ops = -1;\n            return machine;\n        }\n    }\n    return machine;\n}\nexports.runProgram = runProgram;\nfunction printN(v, n) {\n    var bit = Math.pow(2, n);\n    if (v < 0 || v > bit - 1) {\n        throw `${v} doesn't fit in ${n} bits`;\n    }\n    var result = \"\";\n    bit >>= 1;\n    while (bit > 0) {\n        if (v >= bit) {\n            result += \"0\";\n            v -= bit;\n        }\n        else {\n            result += \" \";\n        }\n        bit >>= 1;\n    }\n    return result;\n}\nfunction print4(v) {\n    return printN(v, 4);\n}\nexports.print4 = print4;\nfunction print10(v) {\n    return printN(v, 10);\n}\nexports.print10 = print10;\nfunction populateRow(inst, outer) {\n    var row = print4(inst.op) + \"|\" + print10(inst.data);\n    for (var i = 0; i < row.length; i++) {\n        var e = document.createElement(\"div\");\n        e.style.height = \"10px\";\n        switch (row[i]) {\n            case \"0\":\n                e.style.backgroundColor = \"black\";\n                break;\n            case \"|\":\n                break;\n        }\n        outer.appendChild(e);\n    }\n}\nfunction drawCard(program) {\n    var card = document.getElementById(\"card\");\n    while (card.hasChildNodes()) {\n        card.removeChild(card.firstChild);\n    }\n    program.forEach((i) => populateRow(i, card));\n}\nfunction parseProgram(body, gmMode) {\n    return body\n        .split(\"\\n\")\n        .map(l => {\n        if (gmMode) {\n            var [opV, value] = l.split(\" \");\n            return {\n                op: Ops[opV],\n                data: parseInt(value)\n            };\n        }\n        else {\n            var op = 0;\n            var data = 0;\n            for (var i = 0; i < 4; i++) {\n                op = (op << 1) + (l[i] == \"0\" ? 1 : 0);\n            }\n            for (var i = 5; i < 15; i++) {\n                data = (data << 1) + (l[i] == \"0\" ? 1 : 0);\n            }\n            console.log(op + \"|\" + data);\n            return {\n                op,\n                data\n            };\n        }\n    });\n}\nexports.parseProgram = parseProgram;\nfunction crank() {\n    var code = document.getElementById(\"program\").value;\n    var mode = document.getElementById(\"gm-mode\").checked;\n    var program = parseProgram(code, mode);\n    var registerInputs = [\"r0\", \"r1\", \"r2\", \"r3\", \"r4\", \"r5\", \"r6\", \"r7\"].map(r => document.getElementById(r));\n    var workingInput = document.getElementById(\"w\");\n    var registers = registerInputs.map(r => parseInt(r.value));\n    var working = parseInt(workingInput.value);\n    drawCard(program);\n    var result = runProgram(program, working, ...registers);\n    if (result.ops == -1) {\n        document.getElementById(\"info\").innerText = \"It runs forever\";\n    }\n    else {\n        document.getElementById(\"info\").innerText = `Took ${result.ops} cranks!`;\n    }\n    registerInputs.forEach((v, i) => v.value = (result.registers[i] % 1000).toString());\n    workingInput.value = (result.working % 1000).toString();\n}\nfunction save() {\n    var programBody = document.getElementById(\"program\");\n    var code = programBody.value;\n    var mode = document.getElementById(\"gm-mode\").checked;\n    var program = parseProgram(code, mode);\n    document.getElementById(\"plate\").value = program.map(i => print4(i.op) + \"|\" + print10(i.data)).join(\"\\n\");\n}\nif (typeof document !== 'undefined') {\n    document.getElementById(\"crank\").onclick = crank;\n    document.getElementById(\"save\").onclick = save;\n}\n\n\n//# sourceURL=webpack:///./machine.ts?");

/***/ })

/******/ });