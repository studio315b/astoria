= Licenses

The theme for this site is a heavily modified version of Minima under an link:https://github.com/jekyll/minima/blob/master/LICENSE.txt[MIT license]

++++
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">The Astoria Project</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://astoria.studio315b.com" property="cc:attributionName" rel="cc:attributionURL">Andrew Nichols</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br />Based on <a xmlns:dct="http://purl.org/dc/terms/" href="https://coinsandscrolls.blogspot.com/2019/10/osr-glog-based-homebrew-v2-many-rats-on.html" rel="dct:source">Many Rats On Sticks</a> and other parts of the GLOG Community.
++++

