= Rat

Disgusting little blighters, only a few inches long but full of more diseases than the buy 2 get 1 free brothel.

*AC* -2, *HD* 1/2, *MV* 60, *ATK* bite (1 dmg + disease)

* *Disease:* Bites have a 1 in 20 chance of infecting a disease on the PC.
Diseases start to take hold in 3d6 turns, and leave the victim feverish for 1 week.
* *Pack:* Groups of 5-10 rats share HP (summed together) and attack a single target each turn
* *Afraid of fire:* An attack with a torch will cause the rats to flee for 1d4 rounds.