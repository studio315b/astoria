require 'asciidoctor'
require 'asciidoctor/extensions'

class GlobIncludeProcessor < Asciidoctor::Extensions::IncludeProcessor
  def process doc, reader, target_glob, attributes
    Dir[File.join reader.dir, target_glob].sort.reverse_each do |target|
        if not target.include? 'index.adoc' then
            content = IO.readlines target
            content.unshift '' unless attributes['adjoin-option']
            reader.push_include content, target, target, 1, attributes
        end
    end
    reader
  end

  def handles? target
    target.include? '*'
  end
end

class StatBlockInlineMacro < Asciidoctor::Extensions::InlineMacroProcessor
  use_dsl

  named :stat
  name_positional_attributes 'AC', 'HD', 'ATK'

  def process parent, target, attrs
    text = creature = target.gsub!("-"," ")
    ac = attrs['AC']
    hd = attrs['HD']
    atk = attrs['ATK']
    %{<b\>#{creature}</b\> [DEF #{ac} HD #{hd} ATK #{atk}]}
  end
end

Asciidoctor::Extensions.register do
    include_processor GlobIncludeProcessor
    inline_macro StatBlockInlineMacro
end