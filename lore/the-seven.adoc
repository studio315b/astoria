= The Seven

The Seven are a circle of deities that were uncovered by the Astors during the construction of the Astorian Keep.
Now freed from their bindings, they attempt to seduce new followers and eventually mount an offensive against their captor, Tross.

Unfortunately, the corruption of their influence often prevents their disciples from acting on their will, instead causing them to obsess over their new gods' domain.

Throughout Astoria, the Symbol of the Seven can be found anywhere where cultists of the Seven congregate.

Most disciples choose a single being to dedicate themselves to, but groups are usually mixed, as the Seven believe in cooperation against their enemies.

.Symbol of the Seven
[width="50%", align="center", link=/assets/handouts/sigils.png]
image::/assets/handouts/sigils.png[Sigils of the Seven]

In order clockwise from 12:

* Obot-Bach - Intelligence
* Thedas-Pal - Constitution
* Talin-Zat - Wisdom
* Udor-Lam - Charisma
* Aeto-Krah - Dexterity
* Irad-Maan - Strength - Female
* Banta-Fai - Luck
