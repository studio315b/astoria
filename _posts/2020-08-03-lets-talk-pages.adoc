---
layout: post
date: 2020-08-03
---
= Let\'s Talk Pages

In the last few days, there's been a confluence of uncertainty around the OSR's blogging platform.
link:http://elfmaidsandoctopi.blogspot.com/[Elfmaids & Octopi] suffered a major dataloss, and /u/theHopelessGamer on reddit asked about alternative blog platforms.
I'm taking today to talk about my platform of choice, and the power it provides.

== What is Pages?

Pages, which comes in both link:https://guides.github.com/features/pages/[github] and link:https://docs.gitlab.com/ee/user/project/pages/[gitlab] flavors, is a system by which you can turn *git* repositories into static websites with *jekyll* (or hugo, or gitbook, or others) and *markdown* (or asciidoc in my case.)

I just threw some terms at you, so let's break them down:

Git::
A source-control tool, likely the most popular source control tool in the programming world.
Now you're probably saying something like "I'm not a programmer, I'm not tech-smart, I can't use that!"
But that's where you're wrong.

There are hundreds of tools that make git easy, including an in-browser editor, where you just change and save your files the way you would with any other blogging tool.
In fact, if you're not willing to get down and dirty, you can stop there.
The in-browser editor is great, and good on you for that.

Jekyll::
A Static-Site Generator, written in ruby (you probably don't care) and well supported.
It takes markdown files (we'll get to those in a second) and turns them into nicely formatted web pages.
It also can do other tricks including handling tags, adding SEO metadata, and many other things you likely don't want to have to think about.
The link:https://jekyllrb.com/[documentation] for Jekyll is easy to read, so it's easy to see all you can do with it.

Markdown::
When writing your blog posts, you may be using something like Microsoft Word to format your post (adding titles, underlining text, ect.)
Markdown changes the game by keeping all of your files plain-text, and readable, while being easy to convert to formatted text.
Check out link:https://guides.github.com/features/mastering-markdown/[this great introduction] for more on markdown, or read the link:https://github.github.com/gfm/[complete spec] which lists absolutely everything you can do with github markdown.

== Why All This Fuss?

You may be asking "Why would I go though all this trouble rather than using one of the various other platforms?"

The answer is different for everyone, but here's mine:

=== It's 100% free for 99% of content creators.
If your site takes up less than 500MB, it's free on github, or 4$/month for 2GB, which is a LOT of text, especially if you save your images to an external source such as imgur (which I recommend, git doesn't like non-text files.)

Gitlab instead has a 10GB limit, but charges based on "builds" (that is to say, converting your files from markdown to webpages) of which you get 2000 minutes a month for free, and if you're not making more than 1 post a day, you'll likely never see anything close to that limit.

Even having your blog at a specific site is free (minus the cost of the domain obviously.)

=== Git allows you to work however fits you best.

I'm a programmer by trade, so I work with git all the time, and I have my own workflows.
I like to write my posts from a text editor on my pc, then bulk upload my changes as they're ready (since I mostly do long-form content for Astoria.)
But if you learn more about git, there's all sorts of cool things you can do.

Git also let's you "Undo" a disastrous change safely with the help of reverts, which can be done out of order.

=== Backups are trivial

Just click the download page on any blog repo and it'll be downloaded, in it's entirety, as a zip file.

=== Your repo, your blog

Since all the tools for this process are free (and open-source,) you can easily move to another site, or set up your own self-hosted server whenever you want.
Just install git & jekyll, build your site, and upload the result to wherever you want!

== Don't believe me yet?

So I've pitched everything I can, but I can also offer you this: link:https://gitlab.com/studio315b/astoria[the repo for my blog, in it's entirety].

There you can see how everything is structured, and if you'd like, steal my site completely and use it as the basis for yours.

== A Footnote about Asciidoc

Asciidoc is the souped-up version of markdown, and provides more features, at the expense of some configuration.
I use these features, so it's worth the hassle, but if that's not your speed, markdown does 99% of what most blogs need.